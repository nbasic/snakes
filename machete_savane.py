# -*- encoding: utf-8 -*-

import random
from snake import *

# Barva glave in repa
COLOR_HEAD = '#254117'
COLOR_TAIL = '#347235'

def d(a, b):
    return abs(a[0]-b[0]) + abs(a[1]-b[1])

class MacheteSavane(Snake):

    def __init__(self, field, x, y, dx, dy):
        # Pokličemo konstruktor nadrazreda
        Snake.__init__(self,
                       field=field,
                       color_head=COLOR_HEAD,
                       color_tail=COLOR_TAIL,
                       x=x, y=y, dx=dx, dy=dy)
        # V konstruktor lahko dodate še kakšne atribute.

    def turn(self):
        """
        Igrica pokliče metodo turn vsakič, preden premakne kačo. Kača naj se tu odloči, ali se
        bo obrnila v levo, v desno, ali pa bo nadaljevala pot v isti smeri.

           * v levo se obrne s self.turn_left()
           * v desno se obrne s self.turn_right()
           * koordinate glave so self.coords[0]
           * smer, v katero potuje, je (self.dx, self.dy)
           * spisek koordinat vseh mišk je self.field.mice.keys()
           * spisek vseh kač je self.field.snakes
        """

        k = self.coords[0]
        razd = 0
        c = None
        for m in self.field.mice.keys():
            if not c:
                c = m
                razd = d(k, m)
            else:
                razd1 = d(k, m)
                if razd1 < razd and razd1 != 0:
                    c = m
                    razd = razd1
        if c[0] == m[0]:
            if c[1] > m[1]:
                if self.dx == 1:
                    self.turn_right()
                elif self.dx == -1:
                    self.turn_left()
                elif (self.dx, self.dy) == (0, 1):
                    pass
                else:
                    self.turn_right()
                    self.turn_right()
            else:
                if self.dx == 1:
                    self.turn_left()
                elif self.dx == -1:
                    elf.turn_right()
                elif (self.dx, self.dy) == (0, -1):
                    pass
                else:
                    self.turn_right()
                    self.turn_right()
        else:
            if c[0] > m[0]:
                if self.dy == 1:
                    self.turn_left()
                elif self.dy == -1:
                    self.turn_right()
                elif (self.dx,self.dy) == (1,0):
                    pass
                else:
                    self.turn_right()
                    self.turn_right()
            else:
                if self.dy == 1:
                    self.turn_right()
                elif self.dy == -1:
                    self.turn_left()
                elif (self.dx, self.dy) == (-1,0):
                    pass
                else:
                    self.turn_right()
                    self.turn_right()
            
                
           
##        if random.randint(0, 10) < 5:
##            if random.randint(0, 1) == 1:
##                self.turn_left()
##            else:
##                self.turn_right()
