# -*- encoding: utf-8 -*-

# Glavni program. Deluje s Python 2.x in 3.x.

import sys
import snake
import random

### KONFIFGURACIJA KAČ

# Svojo kačo dodate tako, da tu napišete ustrezni import:
from moja_kacaa import BlackAdder
from bolivian_anaconda import BolivianAnaconda
from congo_water_cobra import CongoWaterCobra
from Smooth_snake import SmoothSnake
from black_adder import BlackAdder2
from parrot_snake import ParrotSnake
from night_snake import NightSnake
from long_nosed_adder import LongNosedAdder
from Boa import Boa
from grozni_kaconcek import GrozniKaconcek
from aqil import Aqil
from dwarf_boa import DwarfBoa
from false_anaconda import FalseAnaconda
from cantors_pitviper import CantorsPitviper
from machete_savane import MacheteSavane
from yarara import Yarara
from hitko import Hitko

# Nato v spisek SNAKES dodate razred, ki predstavlja vašo kačo:

SNAKES = [
    BlackAdder,
    BlackAdder2,
    BolivianAnaconda,
    CongoWaterCobra,
    SmoothSnake,
    ParrotSnake,
    NightSnake,
    LongNosedAdder,
    Boa,
    GrozniKaconcek,
    Aqil,
    DwarfBoa,
    FalseAnaconda,
    CantorsPitviper,
    MacheteSavane,
    Yarara,
    Hitko,
]

# Od tu naprej se ni treba ničesar dotikati

if sys.version.startswith('2'):
    # Import Tkinter, Python 2
    from Tkinter import *
    from ScrolledText import *
else:
    # Import tkinter, Python 3
    from tkinter import *



# Constants definition

class SnakeGame():
    """
    Razred, ki predstavlja celotno igrico.
    """
    def __init__(self, master, snakes, width=snake.WIDTH, height=snake.HEIGHT):
        self.master = master
        self.master.title("Snakes")
        self.width = width
        self.height = height
        self.canvas = Canvas(master, width=width*snake.BLOCK, height=height*snake.BLOCK)
        self.canvas.grid(row=0, column=0)
        self.field = snake.Field(self.canvas, width, height)
        # Postavimo kače v polje
        y = height // 2 - 1
        x = (width - 2 * len(snakes)) // 2
        random.shuffle(snakes)
        for s in snakes:
            self.field.add_snake(s(self.field, x, y, 0, random.choice([-1, 1])))
            x += 2
        self.time = 0
        self.tick()

    def tick(self):
        if self.time % 10 == 0:
            self.field.new_mouse()
        for s in self.field.snakes:
            s.turn()
            s.move()
        self.time += 1
        self.canvas.after(50, self.tick)
        

# Glavni program

root = Tk()
app = SnakeGame(root, SNAKES)
root.mainloop()
