# -*- encoding: utf-8 -*-

import random
from snake import *

# Barva glave in repa
COLOR_HEAD = '#FF3333'
COLOR_TAIL = '#2EB82E' #lahko bi napisala tudi green


class FalseAnaconda(Snake):

    def __init__(self, field, x, y, dx, dy):
        # Pokličemo konstruktor nadrazreda
        Snake.__init__(self,
                       field=field,
                       color_head=COLOR_HEAD,
                       color_tail=COLOR_TAIL,
                       x=x, y=y, dx=dx, dy=dy)
        # V konstruktor lahko dodate še kakšne atribute.

    def turn(self):
        """
        Igrica pokliče metodo turn vsakič, preden premakne kačo. Kača naj se tu odloči, ali se
        bo obrnila v levo, v desno, ali pa bo nadaljevala pot v isti smeri.

           * v levo se obrne s self.turn_left()
           * v desno se obrne s self.turn_right()
           * koordinate glave so self.coords[0]
           * smer, v katero potuje, je (self.dx, self.dy)
           * spisek koordinat vseh mišk je self.field.mice.keys()
           * spisek vseh kač je self.field.snakes
        """
        
        if random.randint(0, 10) < 5:
            self.turn_left()
            self.turn_right()
            self.turn_left()
            self.turn_right()
            self.turn_left()
            self.turn_right()
            
            if random.randint(0, 1) == 1:
                self.turn_left()
            elif random.randint(0, 1) == 0.88:
                self.turn_right()
            else:
                self.turn_right()
